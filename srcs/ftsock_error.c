/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftsock_error.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/14 19:18:37 by garm              #+#    #+#             */
/*   Updated: 2014/05/18 21:32:28 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

int		ftsock_perror(char error)
{
	if (error == FTSOCK_SOCKETERROR)
		ft_putendl(FTSOCK_SOCKETERROR_STR);
	else if (error == FTSOCK_BINDERROR)
		ft_putendl(FTSOCK_BINDERROR_STR);
	else if (error == FTSOCK_LISTENERROR)
		ft_putendl(FTSOCK_LISTENERROR_STR);
	else if (error == FTSOCK_ACCEPTERROR)
		ft_putendl(FTSOCK_ACCEPTERROR_STR);
	else if (error == FTSOCK_CONNECTERROR)
		ft_putendl(FTSOCK_CONNECTERROR_STR);
	else if (error == FTSOCK_MUSTBE_CLIENT)
		ft_putendl(FTSOCK_MUSTBE_CLIENT_STR);
	else if (error == FTSOCK_MUSTBE_SERVER)
		ft_putendl(FTSOCK_MUSTBE_SERVER_STR);
	else if (error == FTSOCK_BAD_PORT)
		ft_putendl(FTSOCK_BAD_PORT_STR);
	return (1);
}
