/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftsock_server.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/14 19:33:58 by garm              #+#    #+#             */
/*   Updated: 2014/05/18 21:34:49 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

t_tcpsock	*ftsock_create_server(int port, int queue_len)
{
	t_tcpsock			*serv;
	struct protoent		*proto;

	proto = getprotobyname("tcp");
	serv = (t_tcpsock *)malloc(sizeof(t_tcpsock));
	serv->ip = NULL;
	serv->port = port;
	serv->queue_len = queue_len;
	serv->type = FTSOCK_SERVER;
	serv->error = FTSOCK_NOERROR;
	if (port == 0)
		serv->error = FTSOCK_BAD_PORT;
	if ((serv->sock = socket(PF_INET, SOCK_STREAM, proto->p_proto)) == -1)
		serv->error = FTSOCK_SOCKETERROR;
	serv->sin.sin_family = AF_INET;
	serv->sin.sin_port = htons(port);
	serv->sin.sin_addr.s_addr = INADDR_ANY;
	if (!serv->error)
	{
		if ((bind(serv->sock, (struct sockaddr *)&serv->sin, SINSIZE)) == -1)
			serv->error = FTSOCK_BINDERROR;
	}
	if (!serv->error && (listen(serv->sock, queue_len)) == -1)
		serv->error = FTSOCK_LISTENERROR;
	return (serv);
}

t_tcpsock	*ftsock_wait(t_tcpsock *s)
{
	unsigned int	csinsize;
	t_tcpsock		*c;

	c = NULL;
	if (s)
	{
		csinsize = sizeof(struct sockaddr_in);
		c = (t_tcpsock *)malloc(sizeof(t_tcpsock));
		c->ip = NULL;
		c->queue_len = 0;
		c->type = FTSOCK_CLIENT;
		c->error = FTSOCK_NOERROR;
		if ((c->sock = accept(s->sock, (t_sockaddr *)&c->sin, &csinsize)) == -1)
			c->error = FTSOCK_ACCEPTERROR;
		c->ip = inet_ntoa(c->sin.sin_addr);
		c->port = ntohs(c->sin.sin_port);
	}
	return (c);
}
