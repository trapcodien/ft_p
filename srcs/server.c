/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/12 16:23:22 by garm              #+#    #+#             */
/*   Updated: 2014/06/20 06:35:53 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	ft_cd(t_tcpsock *client, t_paths *p, char *cmd)
{
	char	**split;
	char	oldpwd[PATH_SIZE];

	split = ft_strsplit(cmd, ' ');
	if (ft_split_len(split) == 1)
	{
		ft_memcpy(p->pwd, p->root, PATH_SIZE);
		chdir(p->root);
		ft_cd_transmit(client, p);
	}
	else if (chdir(split[1]) != -1)
	{
		ft_memcpy(oldpwd, p->pwd, PATH_SIZE);
		getcwd(p->pwd, PATH_SIZE);
		if (ft_strncmp(p->pwd, p->root, ft_strlen(p->root)) != 0)
		{
			ft_memcpy(p->pwd, oldpwd, PATH_SIZE);
			chdir(p->pwd);
		}
		ft_cd_transmit(client, p);
	}
	else
		ft_cd_error(client, split[1]);
	ft_split_destroy(split);
}

int		ft_execute(t_tcpsock *client, char *data, t_paths *p)
{
	if (ft_strcmp(data, "quit") == 0)
	{
		ft_strdel(&data);
		return (0);
	}
	if (ft_strcmp(data, "ls") == 0)
		ft_ls(client);
	else if (ft_strcmp(data, "pwd") == 0)
		ft_pwd(client, p);
	else if (ft_strncmp(data, "cd", 2) == 0)
		ft_cd(client, p, data);
	else
	{
		ft_send(client->sock, "ERROR");
		ft_putstr_fd("'", client->sock);
		ft_putstr_fd(data, client->sock);
		ft_putstr_fd("' is not a valid cmd.", client->sock);
		ft_putchar_fd(CHAR_ETX, client->sock);
	}
	return (1);
}

int		ft_child(t_tcpsock *server, t_tcpsock *client, t_paths *p)
{
	char	*data;

	ft_printf("%s:%i Connected.\n", client->ip, client->port);
	while (ft_recv(client->sock, &data) && ft_execute(client, data, p))
		ft_strdel(&data);
	ft_printf("%s:%i Deconnected.\n", client->ip, client->port);
	ftsock_destroy(&client);
	ftsock_destroy(&server);
	return (0);
}

void	ft_init_paths(t_paths *p)
{
	getcwd(p->root, PATH_SIZE);
	ft_memcpy(p->pwd, p->root, PATH_SIZE);
}

int		main(int argc, char **argv)
{
	t_tcpsock	*server;
	t_tcpsock	*client;
	pid_t		father;
	t_paths		p;

	if (argc != 2)
		return (ft_usage("Usage: ./serveur <port>"));
	ft_init_paths(&p);
	server = ftsock_create(FTSOCK_SERVER, ft_atoi(argv[1]), 10);
	ft_printf("FTP Root => '%s'\nListen on port %i\n\n", p.root, server->port);
	while ((client = ftsock_wait(server)))
	{
		if (ft_tcp_error(client, server))
			exit(1);
		father = fork();
		if (!father)
			return (ft_child(server, client, &p));
		else
		{
			signal(SIGCHLD, SIG_IGN);
			ftsock_destroy(&client);
		}
	}
	ftsock_destroy(&server);
	return (0);
}
