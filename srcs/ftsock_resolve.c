/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftsock_resolve.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/14 19:17:08 by garm              #+#    #+#             */
/*   Updated: 2014/05/18 21:43:08 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

static int	is_ipv4(char **ip)
{
	int		i;
	int		num;

	i = 0;
	if (ft_split_len(ip) == 4)
	{
		while (i < 4)
		{
			if (!ft_strisnum(ip[i]))
				return (0);
			num = (int)ft_atoi(ip[i]);
			if (num < 0 || num > 255)
				return (0);
			i++;
		}
	}
	else
		return (0);
	return (1);
}

void		ftsock_resolve_host(t_tcpsock *host, char *ip)
{
	struct hostent		*he;

	if (!ftsock_is_ipv4_format(ip))
	{
		if ((he = gethostbyname(ip)) == NULL)
			host->error = FTSOCK_UNABLE_RESOLVE;
		else
			ft_memcpy(&(host->sin.sin_addr), he->h_addr_list[0], he->h_length);
	}
	else
		host->sin.sin_addr.s_addr = inet_addr(ip);
}

int			ftsock_is_ipv4_format(char *ip)
{
	int		ipv4;
	char	**split;

	split = ft_strsplit(ip, '.');
	ipv4 = is_ipv4(split);
	ft_split_destroy(split);
	return (ipv4);
}
