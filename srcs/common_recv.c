/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common_recv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 12:47:01 by garm              #+#    #+#             */
/*   Updated: 2014/06/20 06:34:08 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

static char		*ft_str_expand(char *str, int addlen)
{
	int		oldlen;
	int		newlen;
	char	*newstr;

	if (!str)
		return (NULL);
	oldlen = ft_strlen(str);
	newlen = oldlen + addlen;
	newstr = ft_strnew(newlen + 1);
	newstr = ft_strcpy(newstr, str);
	ft_strdel(&str);
	return (newstr);
}

static void		ft_gnl_cut(char *ref, char **s1, char **s2)
{
	int		size1;
	int		size2;

	size1 = ft_strlenc(ref, CHAR_ETX);
	size2 = ft_ban_neg(ft_strlen(ref) - size1 - 1);
	*s1 = ft_strsub(ref, 0, size1);
	*s2 = ft_strsub(ref, size1 + 1, size2);
	ft_strdel(&ref);
}

static t_gnl	*ft_gnl_addfd(int const fd, t_gnl *fdlist)
{
	t_gnl	*newfd;

	newfd = (t_gnl *)ft_memalloc(sizeof(t_gnl));
	newfd->fd = fd;
	newfd->next = fdlist;
	newfd->memory = (char *)ft_memalloc(1);
	return (newfd);
}

static t_gnl	*ft_gnl_init(int const fd, t_gnl *fdlist)
{
	t_gnl	*cursor;
	t_gnl	*tmp;

	cursor = fdlist;
	if (fd == CLEAN)
	{
		while (cursor)
		{
			ft_strdel(&(cursor->memory));
			tmp = cursor;
			cursor = cursor->next;
			ft_memdel((void **)&tmp);
		}
		return (NULL);
	}
	while (cursor)
	{
		if (cursor->fd == fd)
			return (cursor);
		cursor = cursor->next;
	}
	return (ft_gnl_addfd(fd, fdlist));
}

int				ft_recv(int sock, char **data)
{
	char			buf[BUF];
	static t_gnl	*fdlist = NULL;
	char			**memory;
	int				ret;

	if (sock == CLEAN && data == THIS_CRAP)
	{
		fdlist = ft_gnl_init(CLEAN, fdlist);
		return (0);
	}
	if (sock < 0)
		return (-1);
	fdlist = ft_gnl_init(sock, fdlist);
	memory = &(fdlist->memory);
	while (!(ret = ft_strcountc(*memory, CHAR_ETX)) &&
			(ret = read(sock, buf, BUF)) > 0)
	{
		*memory = ft_str_expand(*memory, ret);
		*memory = ft_strncat(*memory, buf, ret);
	}
	if ((ret == -1) || (ret == 0 && **memory == '\0'))
		return (ret);
	ft_gnl_cut(*memory, data, memory);
	return (1);
}
