/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/12 16:22:48 by garm              #+#    #+#             */
/*   Updated: 2014/05/18 21:43:18 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

void	ft_get_response(t_tcpsock *rhost, char *response)
{
	char	*entry;

	entry = NULL;
	ft_putendl(response);
	ft_recv(rhost->sock, &entry);
	ft_putendl(entry);
	ft_memdel((void *)&entry);
}

void	ft_loop(t_tcpsock *rhost)
{
	char	*entry;

	while (ft_putstr("ftp> ") && get_next_line(0, &entry))
	{
		if (ft_strcmp(entry, "") == 0)
		{
			ft_memdel((void *)&entry);
			continue ;
		}
		ft_send(rhost->sock, entry);
		if (ft_strcmp(entry, "quit") == 0)
		{
			ft_putendl("Goodbye.");
			ft_memdel((void *)&entry);
			break ;
		}
		else
		{
			ft_memdel((void *)&entry);
			ft_recv(rhost->sock, &entry);
			ft_get_response(rhost, entry);
			ft_memdel((void *)&entry);
		}
	}
}

int		main(int argc, char **argv)
{
	t_tcpsock	*rhost;

	if (argc != 3)
		return (ft_usage("Usage: ./client <ip/dns> <port>"));
	rhost = ftsock_create(FTSOCK_CLIENT, argv[1], ft_atoi(argv[2]));
	ftsock_connect(rhost);
	if (rhost->error)
		return (ftsock_perror(rhost->error));
	ft_loop(rhost);
	ftsock_destroy(&rhost);
	return (0);
}
