/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 20:47:24 by garm              #+#    #+#             */
/*   Updated: 2014/05/18 20:51:50 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

void	ft_ls(t_tcpsock *client)
{
	pid_t	father;

	father = fork();
	if (father == 0)
	{
		ft_send(client->sock, "SUCCESS");
		dup2(client->sock, 1);
		execl("/bin/ls", "ls", "-l", NULL);
		exit(1);
	}
	wait4(father, NULL, 0, NULL);
	ft_send(client->sock, "");
}

void	ft_transform_path(t_paths *p, char *vpath)
{
	char	*ptr;

	ptr = p->pwd + ft_strlen(p->root);
	ft_strcpy(vpath, "/");
	if (*ptr)
		ft_strcpy(vpath, ptr);
}

void	ft_pwd(t_tcpsock *client, t_paths *p)
{
	char	vpath[PATH_SIZE];

	ft_transform_path(p, vpath);
	ft_send(client->sock, "SUCCESS");
	ft_send(client->sock, vpath);
}

void	ft_cd_transmit(t_tcpsock *client, t_paths *p)
{
	char	vpath[PATH_SIZE];

	ft_transform_path(p, vpath);
	ft_send(client->sock, "SUCCESS");
	ft_send(client->sock, vpath);
}

void	ft_cd_error(t_tcpsock *client, char *badarg)
{
	ft_send(client->sock, "ERROR");
	ft_putstr_fd("Can't cd to '", client->sock);
	ft_putstr_fd(badarg, client->sock);
	ft_putchar_fd('\'', client->sock);
	ft_putchar_fd(CHAR_ETX, client->sock);
}
