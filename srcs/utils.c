/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 21:22:25 by garm              #+#    #+#             */
/*   Updated: 2014/06/20 06:32:40 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

int		ft_usage(char *str)
{
	ft_putendl(str);
	return (1);
}

int		ft_tcp_error(t_tcpsock *client, t_tcpsock *server)
{
	if (server->error)
		return (ftsock_perror(server->error));
	if (client->error)
		return (ftsock_perror(client->error));
	return (0);
}
