/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/12 16:29:18 by garm              #+#    #+#             */
/*   Updated: 2014/06/20 06:33:25 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
# define COMMON_H

# include <unistd.h>
# include <stdarg.h>
# include <stdlib.h>
# include <dirent.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <sys/wait.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <netdb.h>
# include "libft.h"
# include "ftsock.h"

# define PATH_SIZE 4096
# define SINSIZE sizeof(struct sockaddr_in)
# define CHAR_ETX 3

typedef struct				s_paths
{
	char					root[PATH_SIZE];
	char					pwd[PATH_SIZE];
}							t_paths;

/*
** common_recv.c
*/
int							ft_recv(int sock, char **data);

/*
** common.c
*/
void						ft_send(int sock, char *data);

/*
** utils.c
*/
int							ft_usage(char *str);
int							ft_tcp_error(t_tcpsock *client, t_tcpsock *server);

/*
** exec.c
*/
void						ft_ls(t_tcpsock *client);
void						ft_transform_path(t_paths *p, char *vpath);
void						ft_pwd(t_tcpsock *client, t_paths *p);
void						ft_cd_transmit(t_tcpsock *client, t_paths *p);
void						ft_cd_error(t_tcpsock *client, char *badarg);

#endif
